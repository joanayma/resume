#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = u'@joanayma'
SITENAME = u'Joan Aymà'
SITEURL = 'https://cv.joan.ayma.cat'

PATH = 'content'
OUTPUT_PATH = 'public'
CSS_FILE = 'main-2.css'

TIMEZONE = 'Europe/Andorra'

DEFAULT_LANG = u'ca'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (
  ('My blog', 'https://joan.ayma.cat/'),
)
#
# Social widget
#SOCIAL = (('You can add links in your config file', '#'),
#          ('Another social link', '#'),)
#
DEFAULT_PAGINATION = 5

# Uncomment following line if you want document-relative URLs when developing
RELATIVE_URLS = False

###Theme
THEME = "resume"

NAME = 'Joan Aymà'
TAGLINE = 'Tech Lead Devops'
PIC = '4191538.jpeg'

#sidebar links
EMAIL = 'joan@ayma.cat'
WEBSITE = 'cv.joan.ayma.cat'
LINKEDIN = 'joanayma'
GITHUB = 'joanayma'
GITLAB = 'joanayma'

CAREER_SUMMARY = '''
Experience in DevOps, SysOps, Enginering and Site Reliability Enginering. I've been working and developing my skills on Open
Source platforms for more than 10 years. 
<p>
I collaborate on some open-source projects and like to learn and implement new technologies and create new awesome but stable implementations.
<p>
I'm passionate on learning new knwoledgement. I do activites and likes to explain and ask for help. Challenges & coworking is a good combination :)
'''

LANGUAGES = [
	{
		'name': 'English',
		'description': 'Professional',
                'level': '75'
	},
	{
		'name': 'Spanish',
		'description': 'Native',
                'level': '100'
	},
	{
		'name': 'Catalan',
		'description': 'Native',
                'level': '100'
	}
]

TECHNOLOGIES = [
  [str('GNU/Gentoo'),str('50')],
  [str('Archlinux'),str('70')],
  [str('Traefik'),str('70')],
  [str('Windows'),str('50')],
  [str('WDS'),str('55')],
  [str('Apache'),str('80')],
  [str('Nginx'),str('80')],
  [str('libvirtd'),str('70')],
  [str('Proxmox'),str('70')],
  [str('MySQL'),str('70')],
  [str('Docker'),str('80')],
  [str('Kubernetes'),str('90')]
  
]

INTERESTS = [
  [str('Gaming'),str('10')],
  [str('Mountain'),str('15')],
  [str('Climbing'),str('20')],
  [str('Hiking'),str('20')],
  [str('Bike'),str('20')],
  [str('Violin'),str('25')],
  [str('Piano'),str('25')]
]

#PROJECT_INTRO =

#PROJECTS = [
# { 'title' 'tagline'

EXPERIENCES = [
{
 'job_title': 'Tech Lead DevOps',
 'time': 'June 2021 - Present',
 'company': 'Clarivate Analytics, Barcelona',
 'details': 
'''
Design new development cycles for new components and peer review. Help team to follow and improve devops on the different applications.
Maintenance of stardarized widely used pipelines with Jenkins, Docker and Terraform in the Company used from library to AI delivery.
</p>Support developers as a third level.
'''
},
{
 'job_title': 'Senior DevOps',
 'time': 'June 2018 - June 2021',
 'company': 'Clarivate Analytics, Barcelona',
 'details': 
'''
Design automated deployments of new applications in AWS infraestructure, Infraestructure as a Code with terraform, ansible and docker.
</p>Support developers as a third level.
</p>Main goal middle-term to design a global company Kubernetes.
'''
},
{
 'job_title': 'Cloud Architect',
 'time': 'Oct 2016 - June 2018',
 'company': 'ACK Storm, Barcelona',
 'details': 
'''
Design and deployment of customer infraestructure on scalable and high availabilty cloud providers.
Requirements depends on diferents escenarios where we use to minize costs and maxime performance.
'''
},
{
 'job_title':'System Administrator',
 'time': 'June 2014 - Oct 2016',
 'company': 'ACK Storm, Barcelona',
 'details':
'''
Suport and resolve system issues and customer tiquets. Datacenter based services, VPS, LoadBalancing and Monitoing all system services. Most server were GNU/Linux based distributions like CentOS, Debian, Ubuntu. 
Most services are SMTP postfix and dovecot stacks. HTTP stacks were apache, nginx, squid, heartbeat, haproxy. SMTP stacks were postfix and dovecot most of them.
'''
},
{
 'job_title':'System Administrator',
 'time': 'September 2007 - May 2014',
 'company': '''Vall d'Hebron Institut de Recerca -- Setting Consutoria''',
 'details':
'''
Costumer in-house support. The project was to automate all the computers in domain installation. Using software like Windows Deployment Services while keeping all customer software. The diversificated departments (+20) had diference requirements, deployed with Group Policies and Domain computer groups. All the departments tiquets and virus infections decreased.
Another task was keeping the park and servers up and resolving users requests.
'''
}
]

EDUCATIONS = [
{
 'degree': 'IT Engineer',
 'meta': 'Universistat Oberta de Catalunya',
 'time': '2007 - 2014'
},
{
 'degree': 'MCITP and Vmware ESXi 4.1 Field Of Study',
 'meta': 'Softobert',
 'time': '2010'
},
{
 'degree': 'LPIC Level 1',
 'meta': 'Score: 720',
 'time': '2010'
},
{
 'degree': 'Professional IT 2nd degree',
 'meta': 'Institut Obert de Catalunya',
 'time': '2006 - 2008'
},
{
 'degree': 'Bachiller',
 'meta': 'Institut Jaume Balmes',
 'time': '2004 - 2006'
}
]

SKILLS = [
{
 'title': 'Linux', 
 'level': '90', 
 'description': 'GNU/Linux system adminstration & monitoring of more than +2000 machines, physical and virtual.'
},
{
 'title': 'MySQL', 
 'level': '90',
 'description':
 '''Administration & monitoring of mysql servers of +50 customers. Issue resolving and tunning for best performance. Single A, Active-Passive AB, and Active Active ABBA.'''
},
{
 'title': 'Proxmox',
 'level': '75',
 'descripton':
 '''Use of cluster centralizaed with ceph and kvm.'''
},
{
 'title': 'libvirtd+lxc',
 'level': '59',
 'description':
 '''Create and run containers on orquestrator.'''
},
{
 'title': 'libvirt+qemu/kvm',
 'level': '70',
 'description':
 '''Management, definition, provision and performance tunning for various production ready libvirtd+kvm hosts.'''
},
{
 'title': 'MSSQL', 
 'level': '40',
 'description':
 '''Administration & monitoring of SQL.'''
},
{
 'title': 'Google Cloud Platform', 
 'level': '65',
 'description':'Infraestructure and Platform services implementation. Compute, Cluster, AppEngine, Storage, CDN. Customer '
},
{
 'title': 'Amazon Web Services',
 'level': '65',
 'description':'Infraestructure and Platform services implementation. EC2, RDS, ASG, ELB, ALB.'
},
{
 'title': 'Docker', 
 'level': '85',
 'description':'Coding containers and services for distributed services and infraestrucutre. Management and deployment of diverse services. From apache, nodejs, to glusterfs or lsync'
},
{
 'title': 'Docker swarm', 
 'level': '80',
 'description':'Design and managing of clusters on various stacks'
},
{
 'title': 'Kubernetes', 
 'level': '83',
 'description':'''Design kubernetes cluster management and deployment as a powerful orquestrator. Scheduling and autoscaling the cluster nodes. Deployments with canary testing, task crons, blue-green deployments and diferent loadbalancer definitions. 
</p>
I've deployed and managed 5 different clusters, designed and released on production. One gke and four using kops utility.
One of the kops is multiAZ, meaning HA in multizone.
</p>
Good knowledge of Deployments, DaemonSets, ReplicaSets, Secrets, Ingresses (mostly with traefik), Services, Storage providing, and other kind of objects.
Good knowledge of the infraestructure, definitions and autoscaling for pods & nodes.'''
},
{
  'title': 'Terraform',
  'level': '90',
  'description':'''Management of multiple AWS accounts and different environments for different applications using and creating modules, variables and s3 states.
   Git managed'''
},
{
  'title': 'Ansible',
  'level': '80',
  'description':'''Creation of playbooks for deployments and roles for different shared system services. Tests done using molecule and testinfra.'''
},
{
 'title': 'Continuous Integration', 
 'level': '84',
 'description':''
},
{
 'title': 'Jenkins', 
 'level': '80',
 'description':'Experience in deployment and managemnt of diferent plugins of this powerful CI/CD tool. Useful for pipelines from Git repositories, and using environment definitions. Testing is crucial to CD.'
},
{
 'title': 'Gitlab-ci', 
 'level': '80',
 'description':'Pipelines from sources to kubernetes cluster. Deployments of diferents apps and construction of containers while doing some basic testing.'
},
{
 'title': 'Varnish',
 'level': '90',
 'description':'Writen in C with access & threats optimization in mind. Useful to cache HTTP requests'
},
{
 'title': 'Ngnix', 
 'level': '85',
 'description':'High customization and rules for a fast and performance optimized serving. Use of LoadBalancer and caching.'
},
{
 'title': 'Apache', 
 'level': '99',
 'description':'Welknown http server. For a full Optimized and tuned base and example look <a target="_blank" href="https://gitlab.com/joanayma/debian-apache-php">docker/debian-apache-php</a>.'
},
{
 'title': 'Asterisk', 
 'level': '70',
 'description':'Deploying and customizing VoIP service for two companies that required IVRs and AST (PHP) functions. User management with freepbx.'
},
{
 'title': 'Backend Development',
 'level': '20',
 'description': 'Minor coding. Mostly automation oriented.'
},
{
 'title': 'golang', 
 'level': '25',
 'description':'Some projects collaboration submiting Pull Requests for new features.'
},
{
 'title': 'Python', 
 'level': '60',
 'description':'Most of websites created by me and software writen in my repos are in pyhton. Also used for automatization, some libraries and own projects'
},
{
 'title': 'Python Django', 
 'level': '60',
 'description':'Powerful framework with a model/views definitions.'
},
{
 'title': 'Frontend Development',
 'level': '25',
 'description': '''I do some websites like this :)'''
},
{
 'title': 'JS', 
 'level': '30',
 'description':'Frontend coding and use of libraries for some websites.'
},
{
 'title': 'CSS',
 'level': '40',
 'description': 'Comprehesion on classes and id definitions. Creating layouts and learning awesome styles.'
}
]
