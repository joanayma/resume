#!/bin/env python

import pdfkit

options = {
  'print-media-type': '',
  'viewport-size': '1280x1024',
  'page-size': 'A4',
  'orientation': 'Landscape',
  'margin-bottom': '0',
  'margin-left': '0',
  'margin-right': '0',
  'margin-top': '0',
}

pdfkit.from_file('public/index.html', 'public/joan-ayma-cv.pdf', options=options)

